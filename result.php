<!DOCTYPE html>
<html>
<head>
	<title>Trắc Nghiệm</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script src="js/popper.min.js"></script>
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>
<body>
	<?php

	if(empty($_POST)){
		header('Location : index.php');
		exit();
	}
	echo "<pre>";
	print_r($_POST);
	echo "</pre>";
	require_once "function.php";

	if(empty($_POST)) redirect('index.php');

	
	
	$mang = unserialize($_POST['array-data']);




	$xhtml = '';
	if(!empty($mang)){
		$i = 1 ;
		foreach ($mang as $key => $value) {

			$questionId = $_POST['question-' .$value['id']];

			$option0 = check('option-0',$questionId,$value['answer'],$value['option-0']);
			$option1 = check('option-1',$questionId,$value['answer'],$value['option-1']);
			$option2 = check('option-2',$questionId,$value['answer'],$value['option-2']);
			$option3 = check('option-3',$questionId,$value['answer'],$value['option-3']);


			$xhtml .= '
			<div class="form-group">
				<p>'.$i .'.' .$value['question'].'</p>
				<div class="row">
					<div class="col-md-6">'.$option0.$option1.'</div>
					<div class="col-md-6">'.$option2.$option3.'</div>
				</div>
			</div>';
			$i++;
		}
	}	
	?>
	<div class="container list-quiz" style="background-color: #e9e6e2;border: solid 1px;">
		<h1 class="page-header" style="text-align: center;color: blue;">Kết Quả Trắc Nghiệm</h1>
		<form action="#" method="post" name="test-form" id="test-form">
			<?php
			echo $xhtml;
			?>

			<div class="form-group" style="margin-left: 450px;">
				<a href="index.php" class="btn btn-primary">Làm lại</a>
			</div>
		</form>
	</div>
</body>
</html>