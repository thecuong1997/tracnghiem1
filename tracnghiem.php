<!DOCTYPE html>
<html>
<head>
	<title>Trắc Nghiệm</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script src="js/popper.min.js"></script>
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>
<body>
	<a href="logout.php">Đăng Xuất</a>


	<?php
	require_once "function.php";
	$mang = tracnghiem();
	$xhtml = "";
	if(!empty($mang)){
		$i = 1 ;
		foreach ($mang as $key => $value) {

			$option0 = showDapAn($key ,'option-0' ,$value['option-0']);
			$option1 = showDapAn($key ,'option-1' ,$value['option-1']);
			$option2 = showDapAn($key ,'option-2' ,$value['option-2']);
			$option3 = showDapAn($key ,'option-3' ,$value['option-3']);


			$xhtml .= '
				<div class="form-group">
					<p>'.$i .'.' .$value['question'].'</p>
					<div class="row">
					<div class="col-md-6">'.$option0. $option1.'</div>
					<div class="col-md-6">'.$option2. $option3.'</div>
					</div>
				</div>';
			$i++;
		}
	}	
	?>
	<div class="container" style="background-color: #e9e6e2;border: dashed 2px;">
		<h1 style="text-align: center;color: blue;padding-top: 20px;">Trắc Nghiệm</h1>
		<form action="result.php" method="post" name="test-form" id="test-form">
			<?php
			echo $xhtml;
			?>

			<div class="form-group">
				<input type="hidden" name="array-data" value="
				<?php 
					echo  htmlentities(serialize($mang));
				?>;
				"/>

					<button style="margin-left: 450px;"   type="submit" class="btn btn-primary" disabled="disabled"> XEM KẾT QUẢ</button>
				
			</div>
		</form>
	</div>

	<script type="text/javascript">
		$(document).ready(function(){
			$('input[type=radio]').change(function(){
				if($('input[type=radio]:checked').length == 8){
					$('button[type=submit]').removeAttr('disabled');
				}
			});
		});
	</script>

</body>
</html>